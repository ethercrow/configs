module {{ cookiecutter.module_name }}
  ( factorial
  ) where

factorial :: Int -> Int
factorial n = product [1 .. n]
