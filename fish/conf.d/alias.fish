
alias stff='stack test --fast --file-watch'
alias sbff='stack build --fast --file-watch'
alias stf='stack test --fast'
alias sbf='stack build --fast'
alias c='code-insiders -r'
alias m='reattach-to-user-namespace mpv'

alias s='stack --resolver=lts-12.0 --skip-ghc-check exec -- shake'
alias ls='ls -F'
alias ltr='ls -ltr'
alias psg='ps -ef | grep'

alias gs='git status'
alias gd='git diff'
alias gp='git pull'
alias ga='git add -u'
alias gc='git commit'
alias gca='git commit --amend'
alias gco='git checkout'
alias gcob='git checkout -b'
alias gru='git remote update'

alias m='reattach-to-user-namespace mpv'

alias zttp="http -A zign -a zhttp: --default-scheme=https"
alias zk=zkubectl

alias new="cookiecutter --config-file $HOME/.config/cookiecutter/config.yaml"

function mkcd 
    mkdir -p "$argv"
    cd "$argv"
end

alias get_sb_token='zkubectl login stups-test; and export SB_TOKEN=(zkubectl get secret cdp-controller-$USER -o json | jq ".data.\"cluster-token-secret\""| xargs | base64 --decode)'