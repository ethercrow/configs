#!/usr/bin/env bash

set -exu

HERE="$(pwd)"
THERE="$HOME/Library/Application Support/Code - Insiders/User"

rsync "$HERE/"*.json "$THERE/"
rsync "$HERE/snippets/"*.json "$THERE/"