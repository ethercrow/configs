#!/usr/bin/env bash

set -exu

HERE="$(pwd)"
THERE="$HOME/Library/Application Support/Code - Insiders/User"

rsync "$THERE/"*.json "$HERE/"
rsync "$THERE/snippets/"*.json "$HERE/snippets/"